import React, { useEffect, useState } from 'react';
import NavBar from '../../components/NavBar';
import { Card, CardHeader, CardBody, Button, Table } from 'reactstrap';
import ScheduleForm from './ScheduleForm'
import ScheduleRow from './ScheduleRow'
import logo from '../../assets/images/logo.png'
import styled from 'styled-components';
import FooterPage from '../../components/FooterPage';
import moment from 'moment';
import { CSVLink } from 'react-csv';


const Main = styled.div`
img{
  height: 12%;
  width: 15%;
  margin-left: 15%;
  margin-top: 10px;
}

.csvBtn{
        background-color: #f6a10c;
        border: none;
    }
`

const Schedules = () => {

    const [showForm, setShowForm] = useState(false);
    const [schedules, setSchedules] = useState([]);
    const [isEditing, setIsEditing] = useState(false);
    const [scheduleToEdit, setScheduleToEdit] = useState({});
    const [addDone, setAddDone] = useState(false);
    const [allSchedule, setAllSchedules] = useState([]);


    useEffect(() => {
        console.log("sched", schedules)


    }, [schedules])


    useEffect(() => {
        fetch('https://thawing-beach-69628.herokuapp.com/admin/schedules')
            .then(res => res.json())
            .then(res => {
                setSchedules(res);
            })
    }, [addDone])

    const scheduleData = [
        ['ID', 'Fishpond', 'Start Date', 'End Date', 'Capacity', 'Amount', 'Status']
    ]

    allSchedule.forEach(indivSchedule => {
        let indivScheduleData = [];

        indivScheduleData.push(indivSchedule._id);
        indivScheduleData.push(indivSchedule.fishpond.fishpondname);
        indivScheduleData.push(indivSchedule.startDate);
        indivScheduleData.push(indivSchedule.endDate);
        indivScheduleData.push(indivSchedule.capacity);
        indivScheduleData.push(indivSchedule.amount);
        indivScheduleData.push(indivSchedule.isOpen);

        scheduleData.push(indivScheduleData);
    })

    const toggleShowForm = () => {
        // if (showForm) setIsEditing(!isEditing)
        setShowForm(!showForm);
        setIsEditing(false)
        setScheduleToEdit({})
    }

    const saveSchedule = (fishpond, dateRange, capacity, amount) => {
        // console.log(capacity, amount);
        const { from, to } = dateRange;
        const diff = moment(to).diff(moment(from), 'days') + 1;

        if (isEditing) {
            let editedFishpond = fishpond;
            let editedstartDate = from;
            let editedendDate = to;
            let editedCapacity = capacity;
            let editedAmount = amount;

            if (fishpond === null) editedFishpond = scheduleToEdit.fishpond;
            // if (dateRange === "") editedDateRange = scheduleToEdit.dateRange;
            if (capacity === 0) editedCapacity = scheduleToEdit.capacity;
            if (amount === 0) editedAmount = scheduleToEdit.amount;
            if (from === null) editedstartDate = scheduleToEdit.startDate;
            if (to === null) editedendDate = scheduleToEdit.endDate;

            const apiOptions = {
                method: "PATCH",
                headers: { "Content-type": "application/json" },
                body: JSON.stringify({
                    id: scheduleToEdit._id,
                    fishpond: editedFishpond,
                    startDate: editedstartDate,
                    endDate: editedendDate,
                    capacity: parseInt(editedCapacity),
                    amount: parseFloat(editedAmount) * diff

                })
            }
            fetch('https://thawing-beach-69628.herokuapp.com/admin/updateschedule', apiOptions)
                .then(res => res.json())
                .then(res => {
                    let newSchedules = schedules.map(indivSchedule => {
                        if (indivSchedule._id === scheduleToEdit._id) {
                            return res
                        }
                        return indivSchedule;
                    })
                    setSchedules(newSchedules);
                })

        } else {
            const apiOptions = {
                method: "POST",
                headers: { "Content-type": "application/json" },
                body: JSON.stringify({
                    fishpond: fishpond._id,
                    startDate: from,
                    endDate: to,
                    capacity: parseInt(capacity),
                    amount: parseFloat(amount) * diff
                })
            }
            fetch('https://thawing-beach-69628.herokuapp.com/admin/addschedule', apiOptions)
                .then(res => res.json())
                .then(res => {
                    res.fishpond = fishpond;
                    const newSchedules = [res, ...schedules]
                    setSchedules(newSchedules)

                })
        }
        toggleShowForm();
        setScheduleToEdit({})
        setIsEditing(false);
        setAddDone(!addDone)
    }

    const handleEdit = (indivSchedule) => {
        setScheduleToEdit(indivSchedule)
        setIsEditing(true)
    }

    const deleteSchedule = id => {
        const apiOptions = {
            method: "DELETE",
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({ id })
        }
        fetch('https://thawing-beach-69628.herokuapp.com/admin/deleteschedule', apiOptions)
            .then(res => res.json())
            .then(() => {
                const newSchedules = schedules.filter(schedule => {
                    return schedule._id != id
                });
                setSchedules(newSchedules);
                // console.log(newSchedules)
            })
    }

    const updateOpenStatus = schedule => {
        let apiOptions = {
            method: "PATCH",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                id: schedule._id,
                isOpen: !schedule.isOpen
            })
        }
        fetch('https://thawing-beach-69628.herokuapp.com/admin/updateopen', apiOptions)
            .then(res => res.json())
            .then(res => {
                let newSchedules = schedules.map(indivSchedule => {
                    if (schedule._id === indivSchedule._id) {
                        res.fishpond = indivSchedule.fishpond
                        return res
                    }
                    return indivSchedule;
                })
                setSchedules(newSchedules)
            })
    }

    return (
        <>
            <Main>
                <img src={logo} />
                <NavBar />
                <div
                    className='d-flex justify-content-center my-5'
                // style={{ marginLeft: '300px' }}
                >
                    <Card
                        className='col-lg-8'
                        style={{
                            width: '50%',
                            backgroundColor: '#665745'
                        }}
                    >
                        <CardHeader
                            className="text-center"
                            style={{
                                fontSize: '30px',
                                fontWeight: 'bold',
                                backgroundColor: '#665745',
                                color: '#f6a10c'
                            }}
                        >Schedules</CardHeader>
                        <CardHeader>
                            <Button
                                onClick={toggleShowForm}
                                style={{
                                    backgroundColor: '#74b609',
                                    border: 'none'
                                }}
                            >
                                <i class="fa fa-plus" aria-hidden="true"></i> Schedule
                        </Button>
                            <Button
                                className='csvBtn mx-2'
                            >
                                <CSVLink
                                    style={{
                                        color: 'white'
                                    }}
                                    data={scheduleData}
                                    filename={'schedule_reports on' + new Date() + '.csv'}
                                ><i class="fa fa-download" aria-hidden="true"></i> CSV</CSVLink>
                            </Button>
                        </CardHeader>
                        <ScheduleForm
                            showForm={showForm}
                            toggleShowForm={toggleShowForm}
                            saveSchedule={saveSchedule}
                            isEditing={isEditing}
                            scheduleToEdit={scheduleToEdit}
                            setScheduleToEdit={setScheduleToEdit}
                            setIsEditing={setIsEditing}
                        />
                        <CardBody>
                            <Table
                                className='table-stripe text-white'
                            >
                                <thead>
                                    <tr>
                                        <th>Fishpond</th>
                                        <th>Date</th>
                                        <th>Capacity</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {schedules.map(schedule => (
                                        <ScheduleRow
                                            key={schedule._id}
                                            schedule={schedule}
                                            deleteSchedule={deleteSchedule}
                                            handleEdit={handleEdit}
                                            setShowForm={setShowForm}
                                            updateOpenStatus={updateOpenStatus}
                                        />
                                    ))}
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </div>
            </Main>
            <FooterPage />
        </>
    )
}

export default Schedules;