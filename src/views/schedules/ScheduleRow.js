import React from 'react';
import { Button } from 'reactstrap';
import moment from 'moment';

const ScheduleRow = ({ schedule, deleteSchedule, setShowForm, handleEdit, updateOpenStatus }) => {
    // console.log(schedule)
    return (
        <>
            <tr>
                <td>{schedule.fishpond.fishpondname}</td>
                <td>{moment(schedule.startDate).format('LL')} - {moment(schedule.endDate).format('LL')}</td>
                <td>{schedule.capacity}</td>
                <td>{schedule.amount}</td>
                <td
                    onClick={() => updateOpenStatus(schedule)}
                >{schedule.isOpen ? "Open" : "Closed"}</td>
                <td>
                    <Button
                        color='info'
                        className="mx-2"
                        onClick={() => {
                            setShowForm(true)
                            handleEdit(schedule);
                        }}
                        style={{
                            backgroundColor: '#f6a10c',
                            border: 'none'
                        }}
                    ><i class="fa fa-pencil-square" aria-hidden="true"></i></Button>
                    <Button
                        color='danger'
                        onClick={() => deleteSchedule(schedule._id)}
                    ><i class="fa fa-trash-o" aria-hidden="true"></i></Button>
                </td>
            </tr>
        </>
    )
}

export default ScheduleRow;