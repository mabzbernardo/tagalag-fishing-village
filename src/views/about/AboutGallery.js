import React from 'react';
import styled from 'styled-components';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick';
import slide_1 from './images/slide_1.jpg';
import slide_2 from './images/slide_2.jpg';
import slide_3 from './images/slide_3.jpg';
import slide_4 from './images/slide_4.jpg';
import slide_5 from './images/slide_5.jpg';
import slide_6 from './images/slide_6.jpg';
import slide_7 from './images/slide_7.jpg';
import slide_8 from './images/slide_8.jpg';
import slide_9 from './images/slide_9.jpg';
import slide_10 from './images/slide_10.jpg';

const Main = styled.div`
    display: flex;
    justify-content: center;
    width: 100%;
    .slick-slider {
    width: 70%;
    margin: auto;
    }
`

const Image = styled.div`
    background-image: url(${ ({ src }) => src && src});
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    width: 500px;
    height: 500px;
    z-index: 1000;
`


const AboutGallery = () => {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 3000,
        adaptiveHeight: true
    };

    return (
        <Main>
            <Slider {...settings}>
                <Image
                    src={slide_1}
                />
                <Image
                    src={slide_2}
                />
                <Image
                    src={slide_3}
                />
                <Image
                    src={slide_4}
                />
                <Image
                    src={slide_5}
                />
                <Image
                    src={slide_6}
                />
                <Image
                    src={slide_7}
                />
                <Image
                    src={slide_8}
                />
                <Image
                    src={slide_9}
                />
                <Image
                    src={slide_10}
                />
            </Slider>
        </Main>
    )
}

export default AboutGallery;