import React from 'react';
import styled from 'styled-components';
import landingBG from '../assets/images/landingBG.jpg';
import logo from '../assets/images/logo.png';
import { Link } from 'react-router-dom';

const Container = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-around;
    height: 100vh
`

const MainDiv = styled.div`
    background-image: url(${ landingBG});
    background-size: cover;
    border-radius: 1rem;
    box-shadow: 6px 6px 1rem rgba(0,0,0,.5); 
    height: 75%;
    width: 60%;
    margin: 5% 0;
    img{
        height: 100px;
        width: 150px;
        margin: 2%;
    }
    .btn{
        border-style: none;
        outline: none;
        border: 2px solid black;
        background-color: white;
        color: black;
        padding: 10px 32px;
        font-size: 16px;
        cursor: pointer;
        border-color: #efeeea;
        background: transparent;
        transition: all .3s ease-in-out ;
        margin: 2px;
        position: relative;
        left: 350px;
        top: -10px;
        a{
            color: white;
        }
    }
    .btn:hover{
        border-radius: 24px;
        color: white;
    }
    .quote{
        color: white;
        position:relative;
        left:5%;
        top:23%;
        letter-spacing: 1px;
        font-weight: bold;
    }
`

const Landing = () => {
    return (
        <>
            <Container>
                <MainDiv>
                    <img src={logo} />
                    <button className='btn'><Link to='/login'>Login</Link></button>
                    <button className='btn'><Link to='/register'>Register</Link></button>
                    <div className='quote'>
                        <p>Fishing is much more than fish... It is the
                        <br />great ocassion when we may return to the
                        <br />fine simplicity of our forefathers</p>
                        <p>-HERBERT HOOVER</p>
                    </div>
                </MainDiv>
            </Container>
        </>
    )
}

export default Landing;