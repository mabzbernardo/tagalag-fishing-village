import React from 'react';
import Login from './views/pages/Login';


class LoginRegister extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogginActive = true,
        }
    }
    render() {
        const { isLogginActive } = this.state;
        return (
            <div className='App'>
                <div className='login'>
                    <div className='container'>
                        {isLogginActive && <Login />}
                    </div>
                </div>
            </div>
        )
    }
}


export default LoginRegister;