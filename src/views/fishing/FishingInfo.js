import React, { useState } from 'react';
import styled from 'styled-components';
import { Button } from 'reactstrap';

const Container = styled.div`
    display: flex;
    border: 2px solid #f6a10c;
    width: 50%;
    ${'' /* position: absolute; */}
    left: 34%;
    background-color: #665745;
    color: #fff;
`

const Image = styled.div`
    img{
        height: 400px;
        width: 400px;
        margin: 5%;
        border: 10px solid #40362a;
    }
`

const FishingInfo = ({ indivFishPond, deleteFishPond, setShowForm, handleEdit }) => {
    // console.log(indivFishPond)
    return (
        <>
            <tbody>
                <tr>
                    <td>{indivFishPond.fishpondname}</td>
                    <td>{indivFishPond.user.firstName} {indivFishPond.user.lastName}</td>
                    <td>{indivFishPond.description}</td>
                    <td>{indivFishPond.capacity}</td>
                    <td>{indivFishPond.image}</td>
                    <td><Button className="mx-2"
                        style={{
                            backgroundColor: '#f6a10c',
                            border: 'none'
                        }}
                        onClick={() => {
                            setShowForm(true)
                            handleEdit(indivFishPond);
                        }}
                    ><i className="fa fa-pencil-square" aria-hidden="true" style={{ fontSize: '20px' }}></i></Button></td>
                    <td>
                        <Button
                            color='danger'
                            onClick={() => deleteFishPond(indivFishPond._id)}
                        ><i class="fa fa-trash-o" aria-hidden="true"></i></Button>
                    </td>
                </tr>
            </tbody>
        </>
    )
}

export default FishingInfo;