import React from 'react';
import NavBar from '../../components/NavBar';
import logo from '../../assets/images/logo.png'
import styled from 'styled-components';
import FooterPage from '../../components/FooterPage';
import AboutGallery from './AboutGallery'

const Main = styled.div`
img{
  height: 12%;
  width: 15%;
  margin-left: 15%;
  margin-top: 10px;
}
`
const Slick = styled.div`
margin: 50px;
`

const Info = styled.div`
display: flex;
justify-content: center;
align-items: center;
color: #40362a;
font-size: 15px;
div{
    width: 60%;
    span{
        font-weight: bold;
    }
}
`

const About = () => {
    return (
        <>
            <Main>
                <img src={logo} />
                <NavBar />
                <h1 className="text-center"
                    style={{
                        fontSize: '30px',
                        fontWeight: 'bold',
                        color: '#f6a10c',
                        margin: '30px'
                    }}
                >About Us</h1>
                <Slick>
                    <AboutGallery />
                </Slick>

                <Info>
                    <div>
                        <p>Barangay Tagalag was established hundred years ago in which the initial name was <span>“Taga-Ilog”</span> because of it vast expanse of aquatic resources coming from the three connected rivers of Coloong, Polo and Meycauayan.
                 Eventually, the name was changed to <span>Tagalog</span> and finally, <span>Tagalag</span>.<br /><br />
                 Other tales said that the namesake comes from the words <span>“Taga ng Taga”</span> meaning to chop repetitively.<br /><br />

                 The place used to be a large plowing field but during the 1980s, it was erased from the map because of an immense flood.<br /><br />

                 Before, Sityo Bilog and Barangay Bisig are part of Tagalag. The only thing that connects Tagalag and Bilog is the historical wooden bridge of San Isidro Labrador. Because of the flood, residents of Sityo Bilog decided to
                 separate themselves from Tagalag. This was certified on November 20, 1923.<br /><br />

                 Later on, citizens from the threshold of Tagalag decided to create their own barangay which was certified last January 25, 1960. From that day forward, the newly formed barangay was called Bisig.<br /><br />

                 Currently, the 101 hectare barangay has a population of 3,494 residents. Residents celebrate fiesta every May 10 with a fishing contest. Some famous products are mostly fish like Tilapia and Bangus and desserts such as Halaya
                  and Garbanzos. Famous landmarks include resorts such as the Pavillion.
                 </p>
                    </div>
                </Info>
                <FooterPage />
            </Main>
        </>
    )
}

export default About;