import React from 'react';
import ReactLoading from 'react-loading';

const Loader = () => {
    return (
        <div
            className="d-flex justify-content-center align-items-center vh-100"
        >
            <ReactLoading
                color={'#f2ac1f'}
                type={'bars'}
                height={'20%'}
                width={'20%'}
            />
        </div>
    )
}

export default Loader;