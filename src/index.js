import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';

import 'bootstrap/dist/css/bootstrap.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import MainPage from './MainPage';
// import Home from './views/home/Home';
//slick
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
//toast
import 'react-toastify/dist/ReactToastify.css'
import { toast } from 'react-toastify';

toast.configure({
  autoClose: 3000,
  draggable: false,
  hideProgressBar: true,
  newestOnTop: true,
  closeOnClick: true
})



ReactDOM.render(
  // <React.StrictMode>
  <MainPage />,
  // </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
