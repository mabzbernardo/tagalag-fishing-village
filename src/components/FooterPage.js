import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";
import styled from 'styled-components';

const FooterIcon = styled.div`
display: flex;
.icon{
    background-color:#f6a10c;
    color:#fff;
    font-size:15px;
    width:30px;
    height:30px;
    border-radius:50%;
    text-align:center;
    line-height:30px;
    margin:10px 15px 10px 0;
}
p{
    display:inline-block;
    color:#ffffff;
    vertical-align:middle;
    margin:10px 15px 10px 0;
    font-size: 10px;
  letter-spacing: 2px;
}
`

const FooterAbout = styled.div`
color: white;
font-size: 10px;
letter-spacing: 1px;
h5{
    font-weight: bold;
}
`

const SocialIcons = styled.div`
margin-top:20px;
  color:#fff;
a{
    display:inline-block;
  width:30px;
  height:30px;
  cursor:pointer;
  background-color:#f6a10c;
  border-radius:2px;
  font-size:20px;
  color:#ffffff;
  text-align:center;
  line-height:30px;
  margin-right:5px;
  margin-bottom:5px;
}

`

const FooterPage = () => {
    return (
        <>
            <MDBFooter style={{
                backgroundColor: "#665745"
            }}
                className="font-small pt-4 mt-4">
                <MDBContainer fluid className="text-center text-md-left">
                    <MDBRow>
                        <MDBCol md="4" sm="6">
                            <h5
                                className="title"
                                style={{
                                    color: '#f6a10c',
                                    fontSize: '30px'
                                }}
                            >Tagalag Fishing Village</h5>
                            <p
                                style={{
                                    color: 'white'
                                }}
                            >
                                Fishing · Love · Nature</p>
                        </MDBCol>
                        <MDBCol md="4" sm="6">
                            <ul>
                                <li className="list-unstyled">
                                    <FooterIcon>
                                        <i className="fa fa-map-marker icon"></i>
                                        <p>Tagalag, Valenzuela City</p>
                                    </FooterIcon>
                                </li>
                                <li className="list-unstyled">
                                    <FooterIcon>
                                        <i className="fa fa-phone icon"></i>
                                        <p>+028 2783495</p>
                                    </FooterIcon>
                                </li>
                                <li className="list-unstyled">
                                    <FooterIcon>
                                        <i className="fa fa-envelope icon"></i>
                                        <p>support@tagalag.gov</p>
                                    </FooterIcon>
                                </li>
                            </ul>
                        </MDBCol>
                        <MDBCol md="4" sm="6">
                            <FooterAbout>
                                <h5 className="title">About</h5>
                                <p>Tagalag is one of the constituent barangays in the city of Valenzuela,
                                Metro Manila, Philippines. Some famous products are mostly fish like tilapia and bangus and
                                desserts such as halaya and garbanzos. Tagalag is bounded by Polo River in the west,
                            Meycauayan River in the north and Coloong river in the east.</p>
                                <SocialIcons>
                                    <a href="https://www.facebook.com/Barangay-Tagalag-2018-1836544479736591/?__tn__=%2Cd%2CP-R&eid=ARBu2DC9BQmXCT0Fz9jD7zmYUrFtMjMqMY1hJy2IRA6_Jg2Bw90PiqklIdxkcCPPUuwcDILjaS4IHCWE">
                                        <i className="fa fa-facebook-square icon"></i></a>
                                    <a href='https://www.facebook.com/Barangay-Tagalag-2018-1836544479736591/?__tn__=%2Cd%2CP-R&eid=ARBu2DC9BQmXCT0Fz9jD7zmYUrFtMjMqMY1hJy2IRA6_Jg2Bw90PiqklIdxkcCPPUuwcDILjaS4IHCWE'>
                                        <i className="fa fa-twitter-square icon"></i></a>
                                    <a><i className="fa fa-instagram icon"></i></a>
                                </SocialIcons>
                            </FooterAbout>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
                <div className="footer-copyright text-center py-3">
                    <MDBContainer fluid style={{
                        color: "#f6a10c"
                    }}>
                        Mabelle Parogilan © 2020
                    </MDBContainer>
                </div>
            </MDBFooter>
        </>
    );
}

export default FooterPage;