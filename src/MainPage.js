import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Loader from './components/Loader'
import Schedules from './views/schedules/Schedules';

//import pages
const Home = React.lazy(() => import('./views/home/Home'));
const About = React.lazy(() => import('./views/about/About'));
const Fishing = React.lazy(() => import('./views/fishing/Fishing'));
const Schedule = React.lazy(() => import('./views/schedules/Schedules'))
const Booking = React.lazy(() => import('./views/bookings/Bookings'));
const Contact = React.lazy(() => import('./views/contact/Contact'));
const Login = React.lazy(() => import('./views/pages/Login'));
const Register = React.lazy(() => import('./views/pages/Register'));
const Landing = React.lazy(() => import('./components/Landing'));
const MyBooking = React.lazy(() => import('./views/mybookings/MyBookings'));


const MainPage = () => {
    return (
        <>
            <BrowserRouter>
                <React.Suspense
                    fallback={<Loader />}
                >
                    <Switch>
                        <Route
                            exact
                            path="/"
                            render={props => <Landing  {...props} />}
                        />
                        <Route
                            path="/register"
                            render={props => <Register {...props} />}
                        />
                        <Route
                            path="/home"
                            render={props => <Home {...props} />}
                        />
                        <Route
                            path="/login"
                            render={props => <Login {...props} />}
                        />
                        <Route
                            path="/about"
                            render={props => <About {...props} />}
                        />
                        <Route
                            path="/fishing"
                            render={props => <Fishing {...props} />}
                        />
                        <Route
                            path="/schedules"
                            render={props => <Schedule {...props} />}
                        />
                        <Route
                            path="/bookings"
                            render={props => <Booking {...props} />}
                        />
                        <Route
                            path="/mybookings"
                            render={props => <MyBooking {...props} />}
                        />
                        <Route
                            path="/contact"
                            render={props => <Contact {...props} />}
                        />
                    </Switch>
                </React.Suspense>
            </BrowserRouter>
        </>
    )
}

export default MainPage;